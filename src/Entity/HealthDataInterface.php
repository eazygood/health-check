<?php
/**
 * Created by PhpStorm.
 * User: kim
 * Date: 12.03.19
 * Time: 20:07
 */
declare(strict_types=1);
namespace kim\HealthCheckBundle\Entity;

/**
 * Class HealthDataInterface
 * @package kim\HealthCheckBundle\Entity
 */
interface HealthDataInterface
{
    public const STATUS_OK = 1;

    public const STATUS_WARNING = 2;

    public const STATUS_DANGER = 3;

    public const STATUS_CRITICAL = 4;

    public function getStatus(): int;

    public function getAdditionalInfo(): array;
}