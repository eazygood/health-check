<?php
/**
 * Created by PhpStorm.
 * User: kim
 * Date: 12.03.19
 * Time: 20:10
 */
declare(strict_types=1);
namespace kim\HealthCheckBundle\Entity;

/**
 * Class CommonHealthData
 * @package kim\HealthCheckBundle\Entity
 */
class CommonHealthData implements HealthDataInterface
{
    private $status;

    private $additionalInfo = [];

    public function __construct(int $status)
    {
        $this->status = $status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    /**
     * @param array $additionalInfo
     */
    public function setAdditionalInfo(array $additionalInfo): void
    {
        $this->additionalInfo = $additionalInfo;
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function getAdditionalInfo(): array
    {
        return $this->additionalInfo;
    }

}