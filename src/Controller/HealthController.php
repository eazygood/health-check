<?php
/**
 * Created by PhpStorm.
 * User: kim
 * Date: 12.03.19
 * Time: 20:20
 */
declare(strict_types=1);
namespace kim\HealthCheckBundle\Controller;


use kim\HealthCheckBundle\Service\HealthInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class HealthController
 * @package kim\HealthCheckBundle\Controller
 */
class HealthController extends AbstractController
{
    private $healthService = [];

    public function addHealthService(HealthInterface $healthService)
    {
        $this->healthService[] = $healthService;
    }

    /**
     * @Route("/health")
     *
     * @return JsonResponse
     */
    public function getHealth(): JsonResponse
    {
        return $this->json(array_map(function(HealthInterface $healthService) {
            $info = $healthService->getHealthInfo();

            return [
                'name' => $healthService->getName(),
                'info' => [
                    'status' => $info->getStatus(),
                    'additional_info' => $info->getAdditionalInfo()
                ]
            ];
        }, $this->healthService));
    }
}