<?php
/**
 * Created by PhpStorm.
 * User: kim
 * Date: 12.03.19
 * Time: 20:37
 */
declare(strict_types=1);
namespace kim\HealthCheckBundle\DependencyInjection\Compiler;

use kim\HealthCheckBundle\Command\SendDataCommand;
use kim\HealthCheckBundle\Service\HealthInterface;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class HealthServicePath
 * @package kim\HealthCheckBundle\DependencyInjection\Compiler
 */
class HealthServicePath implements CompilerPassInterface
{
    /**
     * @inheritDoc
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->has(HealthInterface::class)) {
            return;
        }

        $controller = $container->findDefinition(HealthInterface::class);
        $commandDefinition = $container->findDefinition(SendDataCommand::class);

        foreach (array_keys($container->findTaggedServiceIds(HealthInterface::TAG)) as $serviceId) {
            $controller->addMethodCall('addHealthService', [new Reference($serviceId)]);
            $commandDefinition->addMethodCall('addHealthService', [new Reference($serviceId)]);
        }
    }
}