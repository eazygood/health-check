<?php
/**
 * Created by PhpStorm.
 * User: kim
 * Date: 12.03.19
 * Time: 19:53
 */
declare(strict_types=1);
namespace kim\HealthCheckBundle\DependencyInjection;

use kim\HealthCheckBundle\Command\SendDataCommand;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Этот файл отвечает за загрузку конфигурационных файлов бандла, создание и регистрацию "definition" сервисов, загрузку параметров в контейнер и т.д.
 *
 * Class HealthCheckExtension
 * @package kim\HealthCheckBundle\DependencyInjection
 */
class HealthCheckExtension extends Extension
{
    /**
     * @inheritDoc
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resource/config'));

        $loader->load('services.yaml');

        // create command difinition
        $commandDifinition = new Definition(SendDataCommand::class);
        // adding references in senders command construct
        foreach ($config['senders'] as $serviceId) {
            $commandDifinition->addArgument(new Reference($serviceId));
        }

        //service registration as console command
        $commandDifinition->addTag('console.command', ['command' => SendDataCommand::COMMAND_NAME]);

        // setting definition into controller
        $container->setDefinition(SendDataCommand::class, $commandDifinition);
    }

}