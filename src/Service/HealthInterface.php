<?php
/**
 * Created by PhpStorm.
 * User: kim
 * Date: 12.03.19
 * Time: 20:14
 */
declare(strict_types=1);
namespace kim\HealthCheckBundle\Service;


use kim\HealthCheckBundle\Entity\HealthDataInterface;

/**
 * Interface HealthInterface
 * @package kim\HealthCheckBundle\Service
 */
interface HealthInterface
{
    public const TAG = 'health.service';

    public function getName(): string;
    public function getHealthInfo(): HealthDataInterface;
}