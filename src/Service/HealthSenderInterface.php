<?php
/**
 * Created by PhpStorm.
 * User: kim
 * Date: 12.03.19
 * Time: 20:51
 */
declare(strict_types=1);
namespace kim\HealthCheckBundle\Service;

/**
 * Interface HealthSenderInterface
 * @package kim\HealthCheckBundle\Service
 */
interface HealthSenderInterface
{
    public function send(array $data): void;
    public function getDescription(): string;
    public function getName(): string;
}