<?php
/**
 * Created by PhpStorm.
 * User: kim
 * Date: 12.03.19
 * Time: 20:53
 */
declare(strict_types=1);
namespace kim\HealthCheckBundle\Command;

use kim\HealthCheckBundle\Entity\HealthDataInterface;
use kim\HealthCheckBundle\Service\HealthInterface;
use kim\HealthCheckBundle\Service\HealthSenderInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class SendDataCommand
 * @package kim\HealthCheckBundle\Command
 */
class SendDataCommand extends Command
{
    public const COMMAND_NAME = 'health:send-info';

    private $senders;

    /**
     * @var $healthService[]
     */
    private $healthService = [];

    /**
     * @var SymfonyStyle
     */
    private $io;

    public function __construct(HealthSenderInterface... $senders)
    {
        parent::__construct(self::COMMAND_NAME);

        $this->senders = $senders;
    }

    public function addHealthService(HealthInterface $healthService)
    {
        $this->healthService[] = $healthService;
    }

    /**
     * @inheritDoc
     */
    protected function configure()
    {
        parent::configure();
        $this->setDescription('Send health data by senders');
    }

    /**
     * @inheritDoc
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        parent::initialize($input, $output);
        $this->io = new SymfonyStyle($input, $output);
    }

    /**
     * @inheritDoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->io->title('Sending health info');

        try {
            $data = array_map(function (HealthInterface $service): HealthDataInterface
            {
               return $service->getHealthInfo();
            }, $this->healthService);

            foreach ($this->senders as $sender) {
                $this->outputInfo($sender);
                $sender->send($data);
            }
        } catch (\Throwable $exception) {
            $this->io->error('Exception occured: ' . $exception->getMessage());
            $this->io->text($exception->getMessage());
        }
    }

    private function outputInfo(HealthSenderInterface $sender)
    {
        if ($name = $sender->getName()) {
            $this->io->writeln($name);
        }

        if ($description = $sender->getDescription()) {
            $this->io->writeln($description);
        }
    }

}